//
//  GameModelTests.swift
//  GameGR1Tests
//
//  Created by Sebastian Guerrero on 11/14/17.
//  Copyright © 2017 SG. All rights reserved.
//

import XCTest
@testable import GameGR1

class GameModelTests: XCTestCase {
  var gameModel:GameModel?
  
  override func setUp() {
    gameModel = GameModel()
  }
  
  func testPuntajeInicialEsCero(){
    XCTAssertEqual(gameModel!.puntaje, 0)
  }
  
  func testRoundInicialEsUno(){
    XCTAssertEqual(gameModel!.ronda, 1)
  }
  
  func testSetObjectivoEsRandom(){
    gameModel!.setObjectivo()
    let objetivo = gameModel!.objetivo
    gameModel!.setObjectivo()
    let objetivo2 = gameModel!.objetivo
    XCTAssert(objetivo != objetivo2)
    XCTAssert(objetivo! > 0)
    XCTAssert(objetivo! < 100)
    
  }
  
  func testObjetivoEsNumero(){
    let objetivo = gameModel!.objetivo
    XCTAssertNotNil(objetivo)
  }
  
  func testJugarActualizaRonda(){
    let rondaActual = gameModel!.ronda
    gameModel?.jugar(valorIntento: 0)
    XCTAssertEqual(gameModel?.ronda, rondaActual + 1)
  }
  
  func testReiniciarRondaUnoPuntajeCero(){
    gameModel!.puntaje = 1000
    gameModel!.ronda = 9
    
    gameModel!.reiniciar()
    
    XCTAssertEqual(gameModel!.puntaje, 0)
    XCTAssertEqual(gameModel!.ronda, 1)
  }
  
  func testCalcularPuntajeJustoEsCien(){
    gameModel!.objetivo = 10
    let puntaje = gameModel!.calcularPuntaje(valorIntento: 10)
    XCTAssertEqual(puntaje, 100)
  }
  
  func testCalcularPuntajeDiferenciaTres(){
    gameModel!.objetivo = 10
    let puntaje = gameModel!.calcularPuntaje(valorIntento: 7)
    let puntaje2 = gameModel!.calcularPuntaje(valorIntento: 13)
    
    XCTAssertEqual(puntaje, 75)
    XCTAssertEqual(puntaje2, 75)
  }
  
  func testCalcularPuntajeDiferenciaDiez(){
    gameModel!.objetivo = 20
    let puntaje = gameModel!.calcularPuntaje(valorIntento: 10)
    let puntaje2 = gameModel!.calcularPuntaje(valorIntento: 30)
    
    XCTAssertEqual(puntaje, 50)
    XCTAssertEqual(puntaje2, 50)
  }
  
  func testCalcularPuntajeCero(){
    gameModel!.objetivo = 20
    let puntaje = gameModel!.calcularPuntaje(valorIntento: 9)
    let puntaje2 = gameModel!.calcularPuntaje(valorIntento: 31)
    
    XCTAssertEqual(puntaje, 0)
    XCTAssertEqual(puntaje2, 0)
  }
  
  func testJugarIncrementaPuntaje(){
    
    gameModel!.objetivo = 20
    gameModel!.jugar(valorIntento: 20)
    gameModel!.objetivo = 31
    gameModel!.jugar(valorIntento: 30)
    
    XCTAssertEqual(gameModel!.puntaje, 175)
  }
  
  func testSetObjetivoDaUnValorAObjectivo(){
    gameModel!.setObjectivo()
    let objetivo = gameModel!.objetivo
    XCTAssertEqual(gameModel!.objetivo, objetivo)
  }
}
















