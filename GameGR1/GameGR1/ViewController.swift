//
//  ViewController.swift
//  GameGR1
//
//  Created by Sebastian Guerrero on 11/14/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  //MARK:- Outlets
  
  @IBOutlet weak var objetivoLabel: UILabel!
  @IBOutlet weak var puntajeLabel: UILabel!
  @IBOutlet weak var rondaLabel: UILabel!
  @IBOutlet weak var slider: UISlider!
  
  //MARK:- ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  //MARK:- Actions

  @IBAction func jugarButtonPressed(_ sender: Any) {
  }
  
  @IBAction func reiniciarButtonPressed(_ sender: Any) {
  }
  
  
  
  
}

